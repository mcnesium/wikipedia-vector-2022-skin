# Wikipedia Vector 2022 Skin

A Webextension, that enables the new Wikipedia Layout and Design by appending '?useskin=vector-2022' to Wikipedia URLs. This is a fork of [Wikipedia Vector Skin][1] by Amanano, that does the opposite.

Develop: use (`web-ext`)[2] to build this Webextension.

[1]: https://addons.mozilla.org/de/firefox/addon/wikipedia-vector-skin
[2]: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Getting_started_with_web-ext
